#!/usr/bin/env python3

import subprocess
from typing import List


def get_current_window_index() -> int:
    cmd = ["tmux", "display-message", "-p", "#I"]
    result = subprocess.run(cmd, stdout=subprocess.PIPE)
    return int(result.stdout.decode())


def get_list_of_windows() -> List[int]:
    cmd = ["tmux", "list-windows", "-F", "#{window_index}"]
    result = subprocess.run(cmd, stdout=subprocess.PIPE)

    # Assumes that the list is already sorted.
    window_list_str = result.stdout.decode().split("\n")

    # Remove the last element as it is empty.
    window_list_str.pop()

    window_list = [int(x) for x in window_list_str]
    return window_list


def move_window(current_index: int, new_index: int):
    cmd = ["tmux", "move-window", "-s", str(current_index), "-t", str(new_index)]
    subprocess.run(cmd)


def select_window(window_index: int):
    cmd = ["tmux", "select-window", "-t", str(window_index)]
    subprocess.run(cmd)


def main():
    initial_window_index = get_current_window_index()
    initial_window_new_index = initial_window_index

    window_list = get_list_of_windows()
    for i, window_index in enumerate(window_list, 1):
        if window_index != i:
            move_window(window_index, i)

            if window_index == initial_window_index:
                initial_window_new_index = i

    # Move back to the initial window.
    select_window(int(initial_window_new_index))


if __name__ == "__main__":
    main()
